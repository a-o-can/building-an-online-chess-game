/****************************************************************************
** Meta object code from reading C++ file 'myclient.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../cpp_19sose_g6/Netzwerk/myclient.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'myclient.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MyClient_t {
    QByteArrayData data[40];
    char stringdata0[383];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MyClient_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MyClient_t qt_meta_stringdata_MyClient = {
    {
QT_MOC_LITERAL(0, 0, 8), // "MyClient"
QT_MOC_LITERAL(1, 9, 10), // "sendVector"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 6), // "vector"
QT_MOC_LITERAL(4, 28, 7), // "vector1"
QT_MOC_LITERAL(5, 36, 7), // "vector2"
QT_MOC_LITERAL(6, 44, 7), // "vector3"
QT_MOC_LITERAL(7, 52, 1), // "a"
QT_MOC_LITERAL(8, 54, 1), // "b"
QT_MOC_LITERAL(9, 56, 14), // "responseToMove"
QT_MOC_LITERAL(10, 71, 8), // "variable"
QT_MOC_LITERAL(11, 80, 9), // "startInfo"
QT_MOC_LITERAL(12, 90, 3), // "wir"
QT_MOC_LITERAL(13, 94, 12), // "gegnernummer"
QT_MOC_LITERAL(14, 107, 8), // "aufgeben"
QT_MOC_LITERAL(15, 116, 16), // "clientDisconnect"
QT_MOC_LITERAL(16, 133, 4), // "remi"
QT_MOC_LITERAL(17, 138, 9), // "okRematch"
QT_MOC_LITERAL(18, 148, 9), // "noRematch"
QT_MOC_LITERAL(19, 158, 4), // "chat"
QT_MOC_LITERAL(20, 163, 9), // "nachricht"
QT_MOC_LITERAL(21, 173, 8), // "sendName"
QT_MOC_LITERAL(22, 182, 4), // "name"
QT_MOC_LITERAL(23, 187, 13), // "lostConnexion"
QT_MOC_LITERAL(24, 201, 11), // "recieveName"
QT_MOC_LITERAL(25, 213, 15), // "connectToServer"
QT_MOC_LITERAL(26, 229, 8), // "readData"
QT_MOC_LITERAL(27, 238, 9), // "writeData"
QT_MOC_LITERAL(28, 248, 19), // "std::vector<quint8>"
QT_MOC_LITERAL(29, 268, 10), // "movevector"
QT_MOC_LITERAL(30, 279, 6), // "signIn"
QT_MOC_LITERAL(31, 286, 5), // "error"
QT_MOC_LITERAL(32, 292, 7), // "rematch"
QT_MOC_LITERAL(33, 300, 14), // "writeOkRematch"
QT_MOC_LITERAL(34, 315, 14), // "writeNoRematch"
QT_MOC_LITERAL(35, 330, 12), // "writeMessage"
QT_MOC_LITERAL(36, 343, 3), // "msg"
QT_MOC_LITERAL(37, 347, 10), // "disconnect"
QT_MOC_LITERAL(38, 358, 9), // "writeName"
QT_MOC_LITERAL(39, 368, 14) // "connetionError"

    },
    "MyClient\0sendVector\0\0vector\0vector1\0"
    "vector2\0vector3\0a\0b\0responseToMove\0"
    "variable\0startInfo\0wir\0gegnernummer\0"
    "aufgeben\0clientDisconnect\0remi\0okRematch\0"
    "noRematch\0chat\0nachricht\0sendName\0"
    "name\0lostConnexion\0recieveName\0"
    "connectToServer\0readData\0writeData\0"
    "std::vector<quint8>\0movevector\0signIn\0"
    "error\0rematch\0writeOkRematch\0"
    "writeNoRematch\0writeMessage\0msg\0"
    "disconnect\0writeName\0connetionError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MyClient[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    6,  134,    2, 0x06 /* Public */,
       9,    1,  147,    2, 0x06 /* Public */,
      11,    3,  150,    2, 0x06 /* Public */,
      14,    0,  157,    2, 0x06 /* Public */,
      15,    0,  158,    2, 0x06 /* Public */,
      16,    0,  159,    2, 0x06 /* Public */,
      17,    0,  160,    2, 0x06 /* Public */,
      18,    0,  161,    2, 0x06 /* Public */,
      19,    1,  162,    2, 0x06 /* Public */,
      21,    1,  165,    2, 0x06 /* Public */,
      23,    0,  168,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      24,    1,  169,    2, 0x0a /* Public */,
      25,    0,  172,    2, 0x0a /* Public */,
      26,    0,  173,    2, 0x0a /* Public */,
      27,    1,  174,    2, 0x0a /* Public */,
      30,    0,  177,    2, 0x0a /* Public */,
      31,    0,  178,    2, 0x0a /* Public */,
      32,    0,  179,    2, 0x0a /* Public */,
      33,    0,  180,    2, 0x0a /* Public */,
      34,    0,  181,    2, 0x0a /* Public */,
      35,    2,  182,    2, 0x0a /* Public */,
      37,    0,  187,    2, 0x0a /* Public */,
      38,    0,  188,    2, 0x0a /* Public */,
      39,    0,  189,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::UChar, QMetaType::UChar, QMetaType::UChar, QMetaType::UChar, QMetaType::Int, QMetaType::Int,    3,    4,    5,    6,    7,    8,
    QMetaType::Void, QMetaType::UChar,   10,
    QMetaType::Void, QMetaType::UChar, QMetaType::UChar, QMetaType::UChar,   12,    3,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QString,   22,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,   22,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,   36,   22,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MyClient::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MyClient *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendVector((*reinterpret_cast< quint8(*)>(_a[1])),(*reinterpret_cast< quint8(*)>(_a[2])),(*reinterpret_cast< quint8(*)>(_a[3])),(*reinterpret_cast< quint8(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 1: _t->responseToMove((*reinterpret_cast< quint8(*)>(_a[1]))); break;
        case 2: _t->startInfo((*reinterpret_cast< quint8(*)>(_a[1])),(*reinterpret_cast< quint8(*)>(_a[2])),(*reinterpret_cast< quint8(*)>(_a[3]))); break;
        case 3: _t->aufgeben(); break;
        case 4: _t->clientDisconnect(); break;
        case 5: _t->remi(); break;
        case 6: _t->okRematch(); break;
        case 7: _t->noRematch(); break;
        case 8: _t->chat((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->sendName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->lostConnexion(); break;
        case 11: _t->recieveName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: _t->connectToServer(); break;
        case 13: _t->readData(); break;
        case 14: _t->writeData((*reinterpret_cast< std::vector<quint8>(*)>(_a[1]))); break;
        case 15: _t->signIn(); break;
        case 16: _t->error(); break;
        case 17: _t->rematch(); break;
        case 18: _t->writeOkRematch(); break;
        case 19: _t->writeNoRematch(); break;
        case 20: _t->writeMessage((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 21: _t->disconnect(); break;
        case 22: _t->writeName(); break;
        case 23: _t->connetionError(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MyClient::*)(quint8 , quint8 , quint8 , quint8 , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::sendVector)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (MyClient::*)(quint8 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::responseToMove)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (MyClient::*)(quint8 , quint8 , quint8 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::startInfo)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (MyClient::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::aufgeben)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (MyClient::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::clientDisconnect)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (MyClient::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::remi)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (MyClient::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::okRematch)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (MyClient::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::noRematch)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (MyClient::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::chat)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (MyClient::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::sendName)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (MyClient::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyClient::lostConnexion)) {
                *result = 10;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MyClient::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_MyClient.data,
    qt_meta_data_MyClient,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MyClient::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MyClient::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MyClient.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int MyClient::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 24;
    }
    return _id;
}

// SIGNAL 0
void MyClient::sendVector(quint8 _t1, quint8 _t2, quint8 _t3, quint8 _t4, int _t5, int _t6)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t4))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t5))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t6))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MyClient::responseToMove(quint8 _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MyClient::startInfo(quint8 _t1, quint8 _t2, quint8 _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MyClient::aufgeben()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void MyClient::clientDisconnect()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void MyClient::remi()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void MyClient::okRematch()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void MyClient::noRematch()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void MyClient::chat(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void MyClient::sendName(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void MyClient::lostConnexion()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
