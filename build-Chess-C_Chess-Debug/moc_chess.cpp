/****************************************************************************
** Meta object code from reading C++ file 'chess.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../cpp_19sose_g6/Logik/chess.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'chess.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Chess_t {
    QByteArrayData data[56];
    char stringdata0[565];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Chess_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Chess_t qt_meta_stringdata_Chess = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Chess"
QT_MOC_LITERAL(1, 6, 11), // "whereToMove"
QT_MOC_LITERAL(2, 18, 0), // ""
QT_MOC_LITERAL(3, 19, 16), // "std::vector<int>"
QT_MOC_LITERAL(4, 36, 8), // "possible"
QT_MOC_LITERAL(5, 45, 10), // "yourPieces"
QT_MOC_LITERAL(6, 56, 7), // "ourMove"
QT_MOC_LITERAL(7, 64, 19), // "std::vector<quint8>"
QT_MOC_LITERAL(8, 84, 6), // "myMove"
QT_MOC_LITERAL(9, 91, 9), // "modifyGui"
QT_MOC_LITERAL(10, 101, 5), // "start"
QT_MOC_LITERAL(11, 107, 3), // "end"
QT_MOC_LITERAL(12, 111, 7), // "schlagt"
QT_MOC_LITERAL(13, 119, 11), // "ourResponse"
QT_MOC_LITERAL(14, 131, 10), // "myResponse"
QT_MOC_LITERAL(15, 142, 5), // "weWin"
QT_MOC_LITERAL(16, 148, 6), // "weLose"
QT_MOC_LITERAL(17, 155, 10), // "errorState"
QT_MOC_LITERAL(18, 166, 8), // "startGui"
QT_MOC_LITERAL(19, 175, 5), // "color"
QT_MOC_LITERAL(20, 181, 17), // "myBauerUmwandlung"
QT_MOC_LITERAL(21, 199, 8), // "position"
QT_MOC_LITERAL(22, 208, 9), // "enPassant"
QT_MOC_LITERAL(23, 218, 18), // "bauerUmwandlungOpp"
QT_MOC_LITERAL(24, 237, 5), // "piece"
QT_MOC_LITERAL(25, 243, 9), // "surrender"
QT_MOC_LITERAL(26, 253, 11), // "mySurrender"
QT_MOC_LITERAL(27, 265, 15), // "weLoseSurrender"
QT_MOC_LITERAL(28, 281, 12), // "hilightCheck"
QT_MOC_LITERAL(29, 294, 10), // "anfgreifer"
QT_MOC_LITERAL(30, 305, 12), // "positionKing"
QT_MOC_LITERAL(31, 318, 11), // "sendOppData"
QT_MOC_LITERAL(32, 330, 12), // "gegnerNummer"
QT_MOC_LITERAL(33, 343, 11), // "gegnerColor"
QT_MOC_LITERAL(34, 355, 9), // "whoseTurn"
QT_MOC_LITERAL(35, 365, 4), // "turn"
QT_MOC_LITERAL(36, 370, 13), // "pointsCounter"
QT_MOC_LITERAL(37, 384, 8), // "myPoints"
QT_MOC_LITERAL(38, 393, 12), // "gegnerPoints"
QT_MOC_LITERAL(39, 406, 8), // "quitGame"
QT_MOC_LITERAL(40, 415, 15), // "moveFromNetwork"
QT_MOC_LITERAL(41, 431, 2), // "sX"
QT_MOC_LITERAL(42, 434, 2), // "sY"
QT_MOC_LITERAL(43, 437, 2), // "zX"
QT_MOC_LITERAL(44, 440, 2), // "zY"
QT_MOC_LITERAL(45, 443, 6), // "status"
QT_MOC_LITERAL(46, 450, 11), // "moveFromGUI"
QT_MOC_LITERAL(47, 462, 6), // "number"
QT_MOC_LITERAL(48, 469, 19), // "responseFromNetwork"
QT_MOC_LITERAL(49, 489, 13), // "otherResponse"
QT_MOC_LITERAL(50, 503, 9), // "setColors"
QT_MOC_LITERAL(51, 513, 5), // "weAre"
QT_MOC_LITERAL(52, 519, 11), // "beginnender"
QT_MOC_LITERAL(53, 531, 13), // "myBauerChange"
QT_MOC_LITERAL(54, 545, 11), // "chosenPiece"
QT_MOC_LITERAL(55, 557, 7) // "rematch"

    },
    "Chess\0whereToMove\0\0std::vector<int>\0"
    "possible\0yourPieces\0ourMove\0"
    "std::vector<quint8>\0myMove\0modifyGui\0"
    "start\0end\0schlagt\0ourResponse\0myResponse\0"
    "weWin\0weLose\0errorState\0startGui\0color\0"
    "myBauerUmwandlung\0position\0enPassant\0"
    "bauerUmwandlungOpp\0piece\0surrender\0"
    "mySurrender\0weLoseSurrender\0hilightCheck\0"
    "anfgreifer\0positionKing\0sendOppData\0"
    "gegnerNummer\0gegnerColor\0whoseTurn\0"
    "turn\0pointsCounter\0myPoints\0gegnerPoints\0"
    "quitGame\0moveFromNetwork\0sX\0sY\0zX\0zY\0"
    "status\0moveFromGUI\0number\0responseFromNetwork\0"
    "otherResponse\0setColors\0weAre\0beginnender\0"
    "myBauerChange\0chosenPiece\0rematch"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Chess[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      17,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  139,    2, 0x06 /* Public */,
       6,    1,  144,    2, 0x06 /* Public */,
       9,    3,  147,    2, 0x06 /* Public */,
      13,    1,  154,    2, 0x06 /* Public */,
      15,    0,  157,    2, 0x06 /* Public */,
      16,    0,  158,    2, 0x06 /* Public */,
      17,    0,  159,    2, 0x06 /* Public */,
      18,    1,  160,    2, 0x06 /* Public */,
      20,    2,  163,    2, 0x06 /* Public */,
      22,    1,  168,    2, 0x06 /* Public */,
      23,    3,  171,    2, 0x06 /* Public */,
      25,    1,  178,    2, 0x06 /* Public */,
      27,    0,  181,    2, 0x06 /* Public */,
      28,    2,  182,    2, 0x06 /* Public */,
      31,    2,  187,    2, 0x06 /* Public */,
      34,    1,  192,    2, 0x06 /* Public */,
      36,    2,  195,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      39,    0,  200,    2, 0x0a /* Public */,
      40,    6,  201,    2, 0x0a /* Public */,
      46,    1,  214,    2, 0x0a /* Public */,
      48,    1,  217,    2, 0x0a /* Public */,
      50,    3,  220,    2, 0x0a /* Public */,
      53,    1,  227,    2, 0x0a /* Public */,
      26,    0,  230,    2, 0x0a /* Public */,
      55,    0,  231,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Bool,   10,   11,   12,
    QMetaType::Void, 0x80000000 | 7,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   21,   19,
    QMetaType::Void, QMetaType::Int,   21,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,   21,   19,   24,
    QMetaType::Void, 0x80000000 | 7,   26,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int,   29,   30,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   32,   33,
    QMetaType::Void, QMetaType::Bool,   35,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   37,   38,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int, QMetaType::Int,   41,   42,   43,   44,   45,   24,
    QMetaType::Void, QMetaType::Int,   47,
    QMetaType::Void, QMetaType::UChar,   49,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,   51,   52,   32,
    QMetaType::Void, QMetaType::Int,   54,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Chess::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Chess *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->whereToMove((*reinterpret_cast< std::vector<int>(*)>(_a[1])),(*reinterpret_cast< std::vector<int>(*)>(_a[2]))); break;
        case 1: _t->ourMove((*reinterpret_cast< std::vector<quint8>(*)>(_a[1]))); break;
        case 2: _t->modifyGui((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 3: _t->ourResponse((*reinterpret_cast< std::vector<quint8>(*)>(_a[1]))); break;
        case 4: _t->weWin(); break;
        case 5: _t->weLose(); break;
        case 6: _t->errorState(); break;
        case 7: _t->startGui((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->myBauerUmwandlung((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 9: _t->enPassant((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->bauerUmwandlungOpp((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 11: _t->surrender((*reinterpret_cast< std::vector<quint8>(*)>(_a[1]))); break;
        case 12: _t->weLoseSurrender(); break;
        case 13: _t->hilightCheck((*reinterpret_cast< std::vector<int>(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 14: _t->sendOppData((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 15: _t->whoseTurn((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->pointsCounter((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 17: _t->quitGame(); break;
        case 18: _t->moveFromNetwork((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 19: _t->moveFromGUI((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->responseFromNetwork((*reinterpret_cast< quint8(*)>(_a[1]))); break;
        case 21: _t->setColors((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 22: _t->myBauerChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->mySurrender(); break;
        case 24: _t->rematch(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Chess::*)(std::vector<int> , std::vector<int> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::whereToMove)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Chess::*)(std::vector<quint8> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::ourMove)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Chess::*)(int , int , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::modifyGui)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Chess::*)(std::vector<quint8> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::ourResponse)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Chess::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::weWin)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Chess::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::weLose)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (Chess::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::errorState)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (Chess::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::startGui)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (Chess::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::myBauerUmwandlung)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (Chess::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::enPassant)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (Chess::*)(int , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::bauerUmwandlungOpp)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (Chess::*)(std::vector<quint8> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::surrender)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (Chess::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::weLoseSurrender)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (Chess::*)(std::vector<int> , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::hilightCheck)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (Chess::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::sendOppData)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (Chess::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::whoseTurn)) {
                *result = 15;
                return;
            }
        }
        {
            using _t = void (Chess::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Chess::pointsCounter)) {
                *result = 16;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Chess::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_Chess.data,
    qt_meta_data_Chess,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Chess::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Chess::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Chess.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Chess::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}

// SIGNAL 0
void Chess::whereToMove(std::vector<int> _t1, std::vector<int> _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Chess::ourMove(std::vector<quint8> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Chess::modifyGui(int _t1, int _t2, bool _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Chess::ourResponse(std::vector<quint8> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Chess::weWin()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void Chess::weLose()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void Chess::errorState()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void Chess::startGui(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Chess::myBauerUmwandlung(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Chess::enPassant(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Chess::bauerUmwandlungOpp(int _t1, int _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Chess::surrender(std::vector<quint8> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void Chess::weLoseSurrender()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void Chess::hilightCheck(std::vector<int> _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void Chess::sendOppData(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void Chess::whoseTurn(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void Chess::pointsCounter(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
