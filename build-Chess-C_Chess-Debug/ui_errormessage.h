/********************************************************************************
** Form generated from reading UI file 'errormessage.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ERRORMESSAGE_H
#define UI_ERRORMESSAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_ErrorMessage
{
public:
    QLineEdit *errorText;
    QPushButton *backToMain;

    void setupUi(QDialog *ErrorMessage)
    {
        if (ErrorMessage->objectName().isEmpty())
            ErrorMessage->setObjectName(QString::fromUtf8("ErrorMessage"));
        ErrorMessage->resize(322, 201);
        errorText = new QLineEdit(ErrorMessage);
        errorText->setObjectName(QString::fromUtf8("errorText"));
        errorText->setGeometry(QRect(10, 10, 301, 121));
        QFont font;
        font.setFamily(QString::fromUtf8("Noto Sans"));
        font.setPointSize(16);
        font.setBold(false);
        font.setWeight(50);
        errorText->setFont(font);
        errorText->setStyleSheet(QString::fromUtf8("color: rgb(252, 1, 7);"));
        errorText->setAlignment(Qt::AlignCenter);
        backToMain = new QPushButton(ErrorMessage);
        backToMain->setObjectName(QString::fromUtf8("backToMain"));
        backToMain->setGeometry(QRect(22, 160, 281, 32));

        retranslateUi(ErrorMessage);

        QMetaObject::connectSlotsByName(ErrorMessage);
    } // setupUi

    void retranslateUi(QDialog *ErrorMessage)
    {
        ErrorMessage->setWindowTitle(QCoreApplication::translate("ErrorMessage", "Dialog", nullptr));
        errorText->setText(QCoreApplication::translate("ErrorMessage", "Something went wrong", nullptr));
        backToMain->setText(QCoreApplication::translate("ErrorMessage", "Dismiss", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ErrorMessage: public Ui_ErrorMessage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ERRORMESSAGE_H
