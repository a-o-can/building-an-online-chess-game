/****************************************************************************
** Meta object code from reading C++ file 'game.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../cpp_19sose_g6/Gui/game.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'game.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Game_t {
    QByteArrayData data[53];
    char stringdata0[526];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Game_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Game_t qt_meta_stringdata_Game = {
    {
QT_MOC_LITERAL(0, 0, 4), // "Game"
QT_MOC_LITERAL(1, 5, 14), // "sendIdentifier"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 10), // "identifier"
QT_MOC_LITERAL(4, 32, 20), // "emitBauernumwandlung"
QT_MOC_LITERAL(5, 53, 4), // "type"
QT_MOC_LITERAL(6, 58, 8), // "quitGame"
QT_MOC_LITERAL(7, 67, 4), // "show"
QT_MOC_LITERAL(8, 72, 10), // "iSurrender"
QT_MOC_LITERAL(9, 83, 10), // "yesrematch"
QT_MOC_LITERAL(10, 94, 7), // "rematch"
QT_MOC_LITERAL(11, 102, 9), // "noRematch"
QT_MOC_LITERAL(12, 112, 8), // "sendName"
QT_MOC_LITERAL(13, 121, 4), // "name"
QT_MOC_LITERAL(14, 126, 9), // "writeName"
QT_MOC_LITERAL(15, 136, 13), // "lostConnexion"
QT_MOC_LITERAL(16, 150, 6), // "points"
QT_MOC_LITERAL(17, 157, 8), // "myPoints"
QT_MOC_LITERAL(18, 166, 9), // "oppPoints"
QT_MOC_LITERAL(19, 176, 17), // "receiveIdentifier"
QT_MOC_LITERAL(20, 194, 12), // "surrenderOwn"
QT_MOC_LITERAL(21, 207, 12), // "surrenderOpp"
QT_MOC_LITERAL(22, 220, 6), // "youWin"
QT_MOC_LITERAL(23, 227, 7), // "youLose"
QT_MOC_LITERAL(24, 235, 18), // "receiveNetworkInfo"
QT_MOC_LITERAL(25, 254, 4), // "port"
QT_MOC_LITERAL(26, 259, 2), // "ip"
QT_MOC_LITERAL(27, 262, 8), // "isServer"
QT_MOC_LITERAL(28, 271, 6), // "begins"
QT_MOC_LITERAL(29, 278, 8), // "startWin"
QT_MOC_LITERAL(30, 287, 8), // "startGui"
QT_MOC_LITERAL(31, 296, 9), // "yourColor"
QT_MOC_LITERAL(32, 306, 5), // "error"
QT_MOC_LITERAL(33, 312, 14), // "setGroupnumber"
QT_MOC_LITERAL(34, 327, 9), // "oppNumber"
QT_MOC_LITERAL(35, 337, 8), // "oppColor"
QT_MOC_LITERAL(36, 346, 9), // "whoseTurn"
QT_MOC_LITERAL(37, 356, 7), // "ourTurn"
QT_MOC_LITERAL(38, 364, 10), // "getNetwork"
QT_MOC_LITERAL(39, 375, 8), // "Network*"
QT_MOC_LITERAL(40, 384, 13), // "getChessBoard"
QT_MOC_LITERAL(41, 398, 11), // "ChessBoard*"
QT_MOC_LITERAL(42, 410, 10), // "updateTime"
QT_MOC_LITERAL(43, 421, 14), // "rematchClicked"
QT_MOC_LITERAL(44, 436, 12), // "rematchNoOpp"
QT_MOC_LITERAL(45, 449, 10), // "rematchOpp"
QT_MOC_LITERAL(46, 460, 10), // "rematchYes"
QT_MOC_LITERAL(47, 471, 9), // "rematchNo"
QT_MOC_LITERAL(48, 481, 17), // "goBackToLastScene"
QT_MOC_LITERAL(49, 499, 4), // "back"
QT_MOC_LITERAL(50, 504, 8), // "openChat"
QT_MOC_LITERAL(51, 513, 6), // "toGame"
QT_MOC_LITERAL(52, 520, 5) // "endIt"

    },
    "Game\0sendIdentifier\0\0identifier\0"
    "emitBauernumwandlung\0type\0quitGame\0"
    "show\0iSurrender\0yesrematch\0rematch\0"
    "noRematch\0sendName\0name\0writeName\0"
    "lostConnexion\0points\0myPoints\0oppPoints\0"
    "receiveIdentifier\0surrenderOwn\0"
    "surrenderOpp\0youWin\0youLose\0"
    "receiveNetworkInfo\0port\0ip\0isServer\0"
    "begins\0startWin\0startGui\0yourColor\0"
    "error\0setGroupnumber\0oppNumber\0oppColor\0"
    "whoseTurn\0ourTurn\0getNetwork\0Network*\0"
    "getChessBoard\0ChessBoard*\0updateTime\0"
    "rematchClicked\0rematchNoOpp\0rematchOpp\0"
    "rematchYes\0rematchNo\0goBackToLastScene\0"
    "back\0openChat\0toGame\0endIt"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Game[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      36,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  194,    2, 0x06 /* Public */,
       4,    1,  197,    2, 0x06 /* Public */,
       6,    1,  200,    2, 0x06 /* Public */,
       8,    0,  203,    2, 0x06 /* Public */,
       9,    0,  204,    2, 0x06 /* Public */,
      10,    0,  205,    2, 0x06 /* Public */,
      11,    0,  206,    2, 0x06 /* Public */,
      12,    1,  207,    2, 0x06 /* Public */,
      14,    0,  210,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      15,    0,  211,    2, 0x0a /* Public */,
      16,    2,  212,    2, 0x0a /* Public */,
      19,    1,  217,    2, 0x0a /* Public */,
      20,    0,  220,    2, 0x0a /* Public */,
      21,    0,  221,    2, 0x0a /* Public */,
      22,    0,  222,    2, 0x0a /* Public */,
      23,    0,  223,    2, 0x0a /* Public */,
      24,    4,  224,    2, 0x0a /* Public */,
      24,    3,  233,    2, 0x2a /* Public | MethodCloned */,
      29,    0,  240,    2, 0x0a /* Public */,
      30,    1,  241,    2, 0x0a /* Public */,
      32,    0,  244,    2, 0x0a /* Public */,
      33,    2,  245,    2, 0x0a /* Public */,
      36,    1,  250,    2, 0x0a /* Public */,
      38,    0,  253,    2, 0x0a /* Public */,
      40,    0,  254,    2, 0x0a /* Public */,
      42,    0,  255,    2, 0x0a /* Public */,
      43,    0,  256,    2, 0x0a /* Public */,
      44,    0,  257,    2, 0x0a /* Public */,
      45,    0,  258,    2, 0x0a /* Public */,
      46,    0,  259,    2, 0x0a /* Public */,
      47,    0,  260,    2, 0x0a /* Public */,
      48,    0,  261,    2, 0x0a /* Public */,
      49,    0,  262,    2, 0x0a /* Public */,
      50,    0,  263,    2, 0x0a /* Public */,
      51,    0,  264,    2, 0x0a /* Public */,
      52,    0,  265,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   13,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   17,   18,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Short, QMetaType::QString, QMetaType::Bool, QMetaType::Bool,   25,   26,   27,   28,
    QMetaType::Void, QMetaType::Short, QMetaType::QString, QMetaType::Bool,   25,   26,   27,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   31,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   34,   35,
    QMetaType::Void, QMetaType::Int,   37,
    0x80000000 | 39,
    0x80000000 | 41,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Game::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Game *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendIdentifier((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->emitBauernumwandlung((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->quitGame((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->iSurrender(); break;
        case 4: _t->yesrematch(); break;
        case 5: _t->rematch(); break;
        case 6: _t->noRematch(); break;
        case 7: _t->sendName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->writeName(); break;
        case 9: _t->lostConnexion(); break;
        case 10: _t->points((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 11: _t->receiveIdentifier((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->surrenderOwn(); break;
        case 13: _t->surrenderOpp(); break;
        case 14: _t->youWin(); break;
        case 15: _t->youLose(); break;
        case 16: _t->receiveNetworkInfo((*reinterpret_cast< qint16(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])),(*reinterpret_cast< bool(*)>(_a[4]))); break;
        case 17: _t->receiveNetworkInfo((*reinterpret_cast< qint16(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 18: _t->startWin(); break;
        case 19: _t->startGui((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->error(); break;
        case 21: _t->setGroupnumber((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 22: _t->whoseTurn((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: { Network* _r = _t->getNetwork();
            if (_a[0]) *reinterpret_cast< Network**>(_a[0]) = std::move(_r); }  break;
        case 24: { ChessBoard* _r = _t->getChessBoard();
            if (_a[0]) *reinterpret_cast< ChessBoard**>(_a[0]) = std::move(_r); }  break;
        case 25: _t->updateTime(); break;
        case 26: _t->rematchClicked(); break;
        case 27: _t->rematchNoOpp(); break;
        case 28: _t->rematchOpp(); break;
        case 29: _t->rematchYes(); break;
        case 30: _t->rematchNo(); break;
        case 31: _t->goBackToLastScene(); break;
        case 32: _t->back(); break;
        case 33: _t->openChat(); break;
        case 34: _t->toGame(); break;
        case 35: _t->endIt(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Game::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Game::sendIdentifier)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Game::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Game::emitBauernumwandlung)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Game::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Game::quitGame)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Game::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Game::iSurrender)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Game::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Game::yesrematch)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Game::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Game::rematch)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (Game::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Game::noRematch)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (Game::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Game::sendName)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (Game::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Game::writeName)) {
                *result = 8;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Game::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_Game.data,
    qt_meta_data_Game,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Game::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Game::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Game.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int Game::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 36)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 36;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 36)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 36;
    }
    return _id;
}

// SIGNAL 0
void Game::sendIdentifier(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Game::emitBauernumwandlung(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Game::quitGame(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Game::iSurrender()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void Game::yesrematch()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void Game::rematch()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void Game::noRematch()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void Game::sendName(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Game::writeName()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
