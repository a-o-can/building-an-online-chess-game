/********************************************************************************
** Form generated from reading UI file 'chat.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHAT_H
#define UI_CHAT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_Chat
{
public:
    QTextEdit *write;
    QPushButton *send;
    QTextBrowser *read;

    void setupUi(QDialog *Chat)
    {
        if (Chat->objectName().isEmpty())
            Chat->setObjectName(QString::fromUtf8("Chat"));
        Chat->resize(562, 511);
        write = new QTextEdit(Chat);
        write->setObjectName(QString::fromUtf8("write"));
        write->setGeometry(QRect(10, 10, 321, 111));
        send = new QPushButton(Chat);
        send->setObjectName(QString::fromUtf8("send"));
        send->setGeometry(QRect(360, 10, 161, 71));
        read = new QTextBrowser(Chat);
        read->setObjectName(QString::fromUtf8("read"));
        read->setGeometry(QRect(10, 130, 321, 361));

        retranslateUi(Chat);

        QMetaObject::connectSlotsByName(Chat);
    } // setupUi

    void retranslateUi(QDialog *Chat)
    {
        Chat->setWindowTitle(QCoreApplication::translate("Chat", "Dialog", nullptr));
        send->setText(QCoreApplication::translate("Chat", "Send", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Chat: public Ui_Chat {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHAT_H
