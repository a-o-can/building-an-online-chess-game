/********************************************************************************
** Form generated from reading UI file 'game.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAME_H
#define UI_GAME_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Game
{
public:
    QStackedWidget *stackedWidget;
    QWidget *page;
    QLabel *label;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_7;
    QLineEdit *group;
    QLineEdit *name;
    QLineEdit *port;
    QLineEdit *ip;
    QHBoxLayout *horizontalLayout_2;
    QGroupBox *groupBox;
    QWidget *horizontalLayoutWidget_4;
    QHBoxLayout *horizontalLayout_5;
    QRadioButton *server;
    QRadioButton *client;
    QHBoxLayout *horizontalLayout_4;
    QGroupBox *groupBox_2;
    QWidget *horizontalLayoutWidget_5;
    QHBoxLayout *horizontalLayout_6;
    QRadioButton *youBegin;
    QRadioButton *opponentBegins;
    QRadioButton *randomBeginner;
    QPushButton *start;
    QPushButton *quit;
    QGroupBox *groupBox_3;
    QLabel *label_33;
    QLabel *label_34;
    QRadioButton *normalTexture;
    QRadioButton *animalTexture;
    QWidget *page_2;
    QWidget *gridLayoutWidget_2;
    QGridLayout *boardPlaceholder_3;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *choiceLayout;
    QLineEdit *groupTop;
    QLabel *label_3;
    QLineEdit *groupBottom;
    QLabel *label_6;
    QWidget *gridLayoutWidget_3;
    QGridLayout *rightGraveyard;
    QLabel *label_2;
    QLineEdit *turnCounterTop;
    QLineEdit *turnCounterBottom;
    QLabel *label_13;
    QLineEdit *timer;
    QLabel *label_14;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_16;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_15;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_31;
    QLabel *label_25;
    QLabel *label_32;
    QLabel *label_27;
    QLabel *label_29;
    QLabel *label_26;
    QLabel *label_28;
    QLabel *label_30;
    QLineEdit *pointsTop;
    QLineEdit *pointsBottom;
    QLabel *label_18;
    QLabel *label_19;
    QLineEdit *colorPlayer;
    QPushButton *quitGame;
    QPushButton *back;
    QPushButton *openChat;
    QWidget *gridLayoutWidget;
    QGridLayout *leftGraveyard;
    QWidget *winScreen;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_8;
    QGridLayout *gifLayout;
    QSpacerItem *horizontalSpacer_9;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_6;
    QVBoxLayout *verticalLayout_4;
    QPushButton *lastSceneWin;
    QPushButton *rematchWin;
    QPushButton *startWin;
    QPushButton *quitWin;
    QSpacerItem *horizontalSpacer_7;
    QWidget *looseScreen;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *verticalLayout_5;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_10;
    QGridLayout *gifLayout_2;
    QSpacerItem *horizontalSpacer_11;
    QSpacerItem *verticalSpacer_4;
    QHBoxLayout *horizontalLayout_12;
    QSpacerItem *horizontalSpacer_12;
    QVBoxLayout *verticalLayout_6;
    QPushButton *lastSceneLoose;
    QPushButton *rematchLoose;
    QPushButton *startLost;
    QPushButton *quitLost;
    QSpacerItem *horizontalSpacer_13;
    QWidget *page_4;
    QLabel *label_4;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_7;
    QPushButton *rematchISurrendered;
    QPushButton *menuSurrenderOwn;
    QPushButton *quitSurrenderOwn;
    QWidget *horizontalLayoutWidget_6;
    QHBoxLayout *gifSurrenderOwn;
    QWidget *page_5;
    QLabel *label_5;
    QWidget *verticalLayoutWidget_6;
    QVBoxLayout *verticalLayout_8;
    QPushButton *rematchOppSurrendered;
    QPushButton *menuSurrenderOpp;
    QPushButton *quitSurrenderOpp;
    QWidget *horizontalLayoutWidget_7;
    QHBoxLayout *gifOppSurrendered;
    QWidget *page_6;
    QLabel *label_17;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *rematchYes;
    QPushButton *rematchNo;
    QWidget *page_3;
    QWidget *verticalLayoutWidget_7;
    QVBoxLayout *verticalLayout_9;
    QHBoxLayout *horizontalLayout_13;
    QSpacerItem *horizontalSpacer_15;
    QVBoxLayout *verticalLayout_10;
    QSpacerItem *verticalSpacer_7;
    QLineEdit *lineEdit_2;
    QSpacerItem *verticalSpacer_8;
    QSpacerItem *horizontalSpacer_16;
    QHBoxLayout *choiceLayout_2;
    QSpacerItem *horizontalSpacer_17;
    QPushButton *quitGame_2;
    QSpacerItem *horizontalSpacer_18;
    QHBoxLayout *horizontalLayout_14;
    QSpacerItem *horizontalSpacer_19;
    QGridLayout *boardPlaceholder_4;
    QSpacerItem *horizontalSpacer_20;
    QPushButton *test_2;
    QWidget *StartScreen;
    QLabel *label_20;
    QPushButton *toGame;
    QLabel *label_21;
    QLabel *label_22;
    QLabel *label_23;
    QLabel *label_24;
    QLabel *label_35;
    QLabel *label_36;
    QLabel *label_37;
    QLabel *label_38;
    QLabel *label_39;
    QLabel *label_40;
    QLabel *label_41;
    QLabel *label_42;
    QPushButton *close;
    QLabel *label_43;
    QLabel *label_44;
    QLabel *label_45;
    QLabel *label_46;
    QLabel *label_47;
    QLabel *label_48;
    QLabel *label_49;
    QLabel *label_50;
    QLabel *label_51;
    QLabel *label_52;
    QLabel *label_53;
    QLabel *label_54;
    QLabel *label_55;
    QLabel *label_56;
    QLabel *label_57;
    QLabel *label_58;
    QLabel *label_59;

    void setupUi(QWidget *Game)
    {
        if (Game->objectName().isEmpty())
            Game->setObjectName(QString::fromUtf8("Game"));
        Game->resize(800, 600);
        stackedWidget = new QStackedWidget(Game);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 801, 601));
        stackedWidget->setStyleSheet(QString::fromUtf8("background-color: rgb(47, 47, 47);"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        label = new QLabel(page);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(290, 10, 251, 81));
        QFont font;
        font.setFamily(QString::fromUtf8("Open Sans Extrabold"));
        font.setPointSize(36);
        font.setBold(true);
        font.setItalic(true);
        font.setWeight(75);
        label->setFont(font);
        label->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);"));
        label->setAlignment(Qt::AlignCenter);
        verticalLayoutWidget_2 = new QWidget(page);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(210, 80, 431, 501));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        group = new QLineEdit(verticalLayoutWidget_2);
        group->setObjectName(QString::fromUtf8("group"));
        group->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        horizontalLayout_7->addWidget(group);

        name = new QLineEdit(verticalLayoutWidget_2);
        name->setObjectName(QString::fromUtf8("name"));
        name->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        horizontalLayout_7->addWidget(name);


        verticalLayout_2->addLayout(horizontalLayout_7);

        port = new QLineEdit(verticalLayoutWidget_2);
        port->setObjectName(QString::fromUtf8("port"));
        port->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_2->addWidget(port);

        ip = new QLineEdit(verticalLayoutWidget_2);
        ip->setObjectName(QString::fromUtf8("ip"));
        ip->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_2->addWidget(ip);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        groupBox = new QGroupBox(verticalLayoutWidget_2);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);\n"
"border-color: rgb(243, 243, 243);"));
        horizontalLayoutWidget_4 = new QWidget(groupBox);
        horizontalLayoutWidget_4->setObjectName(QString::fromUtf8("horizontalLayoutWidget_4"));
        horizontalLayoutWidget_4->setGeometry(QRect(10, 20, 411, 121));
        horizontalLayout_5 = new QHBoxLayout(horizontalLayoutWidget_4);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(20, 0, 0, 0);
        server = new QRadioButton(horizontalLayoutWidget_4);
        server->setObjectName(QString::fromUtf8("server"));
        server->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);"));

        horizontalLayout_5->addWidget(server);

        client = new QRadioButton(horizontalLayoutWidget_4);
        client->setObjectName(QString::fromUtf8("client"));
        client->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);"));

        horizontalLayout_5->addWidget(client);


        horizontalLayout_2->addWidget(groupBox);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        groupBox_2 = new QGroupBox(verticalLayoutWidget_2);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);\n"
"border-color: rgb(243, 243, 243);"));
        horizontalLayoutWidget_5 = new QWidget(groupBox_2);
        horizontalLayoutWidget_5->setObjectName(QString::fromUtf8("horizontalLayoutWidget_5"));
        horizontalLayoutWidget_5->setGeometry(QRect(10, 20, 411, 121));
        horizontalLayout_6 = new QHBoxLayout(horizontalLayoutWidget_5);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(20, 0, 0, 0);
        youBegin = new QRadioButton(horizontalLayoutWidget_5);
        youBegin->setObjectName(QString::fromUtf8("youBegin"));
        youBegin->setEnabled(true);
        youBegin->setTabletTracking(false);
        youBegin->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);"));
        youBegin->setChecked(true);

        horizontalLayout_6->addWidget(youBegin);

        opponentBegins = new QRadioButton(horizontalLayoutWidget_5);
        opponentBegins->setObjectName(QString::fromUtf8("opponentBegins"));
        opponentBegins->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);"));

        horizontalLayout_6->addWidget(opponentBegins);

        randomBeginner = new QRadioButton(horizontalLayoutWidget_5);
        randomBeginner->setObjectName(QString::fromUtf8("randomBeginner"));
        randomBeginner->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);"));

        horizontalLayout_6->addWidget(randomBeginner);


        horizontalLayout_4->addWidget(groupBox_2);


        verticalLayout_2->addLayout(horizontalLayout_4);

        start = new QPushButton(verticalLayoutWidget_2);
        start->setObjectName(QString::fromUtf8("start"));
        start->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_2->addWidget(start);

        quit = new QPushButton(verticalLayoutWidget_2);
        quit->setObjectName(QString::fromUtf8("quit"));
        quit->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_2->addWidget(quit);

        groupBox_3 = new QGroupBox(page);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(19, 70, 171, 511));
        groupBox_3->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);\n"
"border-color: rgb(243, 243, 243);"));
        label_33 = new QLabel(groupBox_3);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setGeometry(QRect(80, 60, 81, 91));
        label_33->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/king_black_normal.png);"));
        label_34 = new QLabel(groupBox_3);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setGeometry(QRect(80, 240, 81, 81));
        label_34->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/king_white.png);"));
        normalTexture = new QRadioButton(groupBox_3);
        normalTexture->setObjectName(QString::fromUtf8("normalTexture"));
        normalTexture->setGeometry(QRect(10, 100, 71, 20));
        normalTexture->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);"));
        animalTexture = new QRadioButton(groupBox_3);
        animalTexture->setObjectName(QString::fromUtf8("animalTexture"));
        animalTexture->setGeometry(QRect(10, 280, 81, 20));
        animalTexture->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);"));
        animalTexture->setChecked(true);
        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        gridLayoutWidget_2 = new QWidget(page_2);
        gridLayoutWidget_2->setObjectName(QString::fromUtf8("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(150, 90, 511, 481));
        boardPlaceholder_3 = new QGridLayout(gridLayoutWidget_2);
        boardPlaceholder_3->setObjectName(QString::fromUtf8("boardPlaceholder_3"));
        boardPlaceholder_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayoutWidget = new QWidget(page_2);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(200, 0, 401, 80));
        choiceLayout = new QHBoxLayout(horizontalLayoutWidget);
        choiceLayout->setObjectName(QString::fromUtf8("choiceLayout"));
        choiceLayout->setContentsMargins(0, 0, 0, 0);
        groupTop = new QLineEdit(page_2);
        groupTop->setObjectName(QString::fromUtf8("groupTop"));
        groupTop->setGeometry(QRect(20, 30, 50, 50));
        groupTop->setMinimumSize(QSize(0, 50));
        groupTop->setMaximumSize(QSize(50, 50));
        groupTop->setFont(font);
        groupTop->setStyleSheet(QString::fromUtf8("background-color: rgb(98, 98, 98);\n"
"color: rgb(239, 239, 239);"));
        groupTop->setAlignment(Qt::AlignCenter);
        label_3 = new QLabel(page_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(20, 10, 61, 20));
        label_3->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));
        groupBottom = new QLineEdit(page_2);
        groupBottom->setObjectName(QString::fromUtf8("groupBottom"));
        groupBottom->setGeometry(QRect(20, 500, 50, 50));
        groupBottom->setMinimumSize(QSize(0, 50));
        groupBottom->setMaximumSize(QSize(50, 50));
        groupBottom->setFont(font);
        groupBottom->setStyleSheet(QString::fromUtf8("background-color: rgb(98, 98, 98);\n"
"color: rgb(239, 239, 239);"));
        groupBottom->setAlignment(Qt::AlignCenter);
        label_6 = new QLabel(page_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(20, 480, 59, 16));
        label_6->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));
        gridLayoutWidget_3 = new QWidget(page_2);
        gridLayoutWidget_3->setObjectName(QString::fromUtf8("gridLayoutWidget_3"));
        gridLayoutWidget_3->setGeometry(QRect(670, 120, 121, 361));
        rightGraveyard = new QGridLayout(gridLayoutWidget_3);
        rightGraveyard->setObjectName(QString::fromUtf8("rightGraveyard"));
        rightGraveyard->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(page_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(70, 10, 71, 20));
        label_2->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));
        label_2->setAlignment(Qt::AlignCenter);
        turnCounterTop = new QLineEdit(page_2);
        turnCounterTop->setObjectName(QString::fromUtf8("turnCounterTop"));
        turnCounterTop->setGeometry(QRect(80, 30, 50, 50));
        turnCounterTop->setMaximumSize(QSize(50, 50));
        turnCounterTop->setFont(font);
        turnCounterTop->setStyleSheet(QString::fromUtf8("background-color: rgb(98, 98, 98);\n"
"color: rgb(239, 239, 239);"));
        turnCounterTop->setAlignment(Qt::AlignCenter);
        turnCounterBottom = new QLineEdit(page_2);
        turnCounterBottom->setObjectName(QString::fromUtf8("turnCounterBottom"));
        turnCounterBottom->setGeometry(QRect(80, 500, 50, 50));
        turnCounterBottom->setMaximumSize(QSize(50, 50));
        turnCounterBottom->setFont(font);
        turnCounterBottom->setStyleSheet(QString::fromUtf8("background-color: rgb(98, 98, 98);\n"
"color: rgb(239, 239, 239);"));
        turnCounterBottom->setAlignment(Qt::AlignCenter);
        label_13 = new QLabel(page_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(80, 480, 51, 20));
        label_13->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));
        label_13->setAlignment(Qt::AlignCenter);
        timer = new QLineEdit(page_2);
        timer->setObjectName(QString::fromUtf8("timer"));
        timer->setGeometry(QRect(660, 20, 91, 50));
        timer->setMaximumSize(QSize(100, 50));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Open Sans Extrabold"));
        font1.setPointSize(26);
        font1.setBold(true);
        font1.setItalic(true);
        font1.setWeight(75);
        timer->setFont(font1);
        timer->setStyleSheet(QString::fromUtf8("background-color: rgb(98, 98, 98);\n"
"color: rgb(239, 239, 239);"));
        timer->setAlignment(Qt::AlignCenter);
        label_14 = new QLabel(page_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(680, 0, 59, 16));
        label_14->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));
        horizontalLayoutWidget_2 = new QWidget(page_2);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(180, 570, 481, 31));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(horizontalLayoutWidget_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Arial"));
        font2.setBold(true);
        font2.setWeight(75);
        label_7->setFont(font2);
        label_7->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        horizontalLayout->addWidget(label_7);

        label_8 = new QLabel(horizontalLayoutWidget_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font2);
        label_8->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        horizontalLayout->addWidget(label_8);

        label_9 = new QLabel(horizontalLayoutWidget_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font2);
        label_9->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        horizontalLayout->addWidget(label_9);

        label_10 = new QLabel(horizontalLayoutWidget_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font2);
        label_10->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        horizontalLayout->addWidget(label_10);

        label_16 = new QLabel(horizontalLayoutWidget_2);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setFont(font2);
        label_16->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        horizontalLayout->addWidget(label_16);

        label_11 = new QLabel(horizontalLayoutWidget_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setFont(font2);
        label_11->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        horizontalLayout->addWidget(label_11);

        label_12 = new QLabel(horizontalLayoutWidget_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setFont(font2);
        label_12->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        horizontalLayout->addWidget(label_12);

        label_15 = new QLabel(horizontalLayoutWidget_2);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font2);
        label_15->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        horizontalLayout->addWidget(label_15);

        verticalLayoutWidget = new QWidget(page_2);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(130, 90, 21, 471));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_31 = new QLabel(verticalLayoutWidget);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setFont(font2);
        label_31->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        verticalLayout->addWidget(label_31);

        label_25 = new QLabel(verticalLayoutWidget);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setFont(font2);
        label_25->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        verticalLayout->addWidget(label_25);

        label_32 = new QLabel(verticalLayoutWidget);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setFont(font2);
        label_32->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        verticalLayout->addWidget(label_32);

        label_27 = new QLabel(verticalLayoutWidget);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setFont(font2);
        label_27->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        verticalLayout->addWidget(label_27);

        label_29 = new QLabel(verticalLayoutWidget);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setFont(font2);
        label_29->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        verticalLayout->addWidget(label_29);

        label_26 = new QLabel(verticalLayoutWidget);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setFont(font2);
        label_26->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        verticalLayout->addWidget(label_26);

        label_28 = new QLabel(verticalLayoutWidget);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setFont(font2);
        label_28->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        verticalLayout->addWidget(label_28);

        label_30 = new QLabel(verticalLayoutWidget);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setFont(font2);
        label_30->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));

        verticalLayout->addWidget(label_30);

        pointsTop = new QLineEdit(page_2);
        pointsTop->setObjectName(QString::fromUtf8("pointsTop"));
        pointsTop->setGeometry(QRect(730, 70, 41, 41));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Open Sans Extrabold"));
        font3.setPointSize(20);
        font3.setBold(true);
        font3.setItalic(true);
        font3.setWeight(75);
        pointsTop->setFont(font3);
        pointsTop->setStyleSheet(QString::fromUtf8("background-color: rgb(47, 47, 47);\n"
"color: rgb(239, 239, 239);"));
        pointsTop->setAlignment(Qt::AlignCenter);
        pointsBottom = new QLineEdit(page_2);
        pointsBottom->setObjectName(QString::fromUtf8("pointsBottom"));
        pointsBottom->setGeometry(QRect(70, 550, 41, 41));
        pointsBottom->setFont(font3);
        pointsBottom->setStyleSheet(QString::fromUtf8("background-color: rgb(47, 47, 47);\n"
"color: rgb(239, 239, 239);"));
        pointsBottom->setAlignment(Qt::AlignCenter);
        label_18 = new QLabel(page_2);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setGeometry(QRect(20, 560, 41, 18));
        label_18->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));
        label_19 = new QLabel(page_2);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setGeometry(QRect(680, 80, 41, 18));
        label_19->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(239, 239, 239);"));
        colorPlayer = new QLineEdit(page_2);
        colorPlayer->setObjectName(QString::fromUtf8("colorPlayer"));
        colorPlayer->setGeometry(QRect(670, 490, 121, 41));
        QFont font4;
        font4.setPointSize(12);
        font4.setBold(false);
        font4.setWeight(50);
        colorPlayer->setFont(font4);
        colorPlayer->setStyleSheet(QString::fromUtf8("background-color: rgb(47, 47, 47);\n"
"color: rgb(239, 239, 239);"));
        quitGame = new QPushButton(page_2);
        quitGame->setObjectName(QString::fromUtf8("quitGame"));
        quitGame->setGeometry(QRect(670, 540, 121, 51));
        quitGame->setFont(font2);
        quitGame->setStyleSheet(QString::fromUtf8("background-color: rgb(98, 98, 98);\n"
"color: rgb(239, 239, 239);"));
        back = new QPushButton(page_2);
        back->setObjectName(QString::fromUtf8("back"));
        back->setGeometry(QRect(670, 513, 121, 41));
        back->setStyleSheet(QString::fromUtf8("background-color: rgb(98, 98, 98);\n"
"color: rgb(239, 239, 239);"));
        openChat = new QPushButton(page_2);
        openChat->setObjectName(QString::fromUtf8("openChat"));
        openChat->setGeometry(QRect(10, 90, 111, 21));
        openChat->setStyleSheet(QString::fromUtf8("background-color: rgb(98, 98, 98);\n"
"color: rgb(239, 239, 239);"));
        gridLayoutWidget = new QWidget(page_2);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 120, 121, 361));
        leftGraveyard = new QGridLayout(gridLayoutWidget);
        leftGraveyard->setObjectName(QString::fromUtf8("leftGraveyard"));
        leftGraveyard->setContentsMargins(0, 0, 0, 0);
        stackedWidget->addWidget(page_2);
        winScreen = new QWidget();
        winScreen->setObjectName(QString::fromUtf8("winScreen"));
        verticalLayoutWidget_3 = new QWidget(winScreen);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(0, 0, 801, 601));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalSpacer_2 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Maximum);

        verticalLayout_3->addItem(verticalSpacer_2);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_8 = new QSpacerItem(100, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_8);

        gifLayout = new QGridLayout();
        gifLayout->setObjectName(QString::fromUtf8("gifLayout"));

        horizontalLayout_9->addLayout(gifLayout);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_9);


        verticalLayout_3->addLayout(horizontalLayout_9);

        verticalSpacer = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_3->addItem(verticalSpacer);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_6 = new QSpacerItem(200, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_6);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        lastSceneWin = new QPushButton(verticalLayoutWidget_3);
        lastSceneWin->setObjectName(QString::fromUtf8("lastSceneWin"));
        lastSceneWin->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_4->addWidget(lastSceneWin);

        rematchWin = new QPushButton(verticalLayoutWidget_3);
        rematchWin->setObjectName(QString::fromUtf8("rematchWin"));
        rematchWin->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_4->addWidget(rematchWin);

        startWin = new QPushButton(verticalLayoutWidget_3);
        startWin->setObjectName(QString::fromUtf8("startWin"));
        startWin->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_4->addWidget(startWin);

        quitWin = new QPushButton(verticalLayoutWidget_3);
        quitWin->setObjectName(QString::fromUtf8("quitWin"));
        quitWin->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_4->addWidget(quitWin);


        horizontalLayout_10->addLayout(verticalLayout_4);

        horizontalSpacer_7 = new QSpacerItem(200, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_7);


        verticalLayout_3->addLayout(horizontalLayout_10);

        stackedWidget->addWidget(winScreen);
        looseScreen = new QWidget();
        looseScreen->setObjectName(QString::fromUtf8("looseScreen"));
        verticalLayoutWidget_5 = new QWidget(looseScreen);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(0, 0, 801, 601));
        verticalLayout_5 = new QVBoxLayout(verticalLayoutWidget_5);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        verticalSpacer_3 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Maximum);

        verticalLayout_5->addItem(verticalSpacer_3);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalSpacer_10 = new QSpacerItem(100, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_10);

        gifLayout_2 = new QGridLayout();
        gifLayout_2->setObjectName(QString::fromUtf8("gifLayout_2"));

        horizontalLayout_11->addLayout(gifLayout_2);

        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_11);


        verticalLayout_5->addLayout(horizontalLayout_11);

        verticalSpacer_4 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_5->addItem(verticalSpacer_4);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalSpacer_12 = new QSpacerItem(200, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_12);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        lastSceneLoose = new QPushButton(verticalLayoutWidget_5);
        lastSceneLoose->setObjectName(QString::fromUtf8("lastSceneLoose"));
        lastSceneLoose->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_6->addWidget(lastSceneLoose);

        rematchLoose = new QPushButton(verticalLayoutWidget_5);
        rematchLoose->setObjectName(QString::fromUtf8("rematchLoose"));
        rematchLoose->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_6->addWidget(rematchLoose);

        startLost = new QPushButton(verticalLayoutWidget_5);
        startLost->setObjectName(QString::fromUtf8("startLost"));
        startLost->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_6->addWidget(startLost);

        quitLost = new QPushButton(verticalLayoutWidget_5);
        quitLost->setObjectName(QString::fromUtf8("quitLost"));
        quitLost->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);\n"
""));

        verticalLayout_6->addWidget(quitLost);


        horizontalLayout_12->addLayout(verticalLayout_6);

        horizontalSpacer_13 = new QSpacerItem(200, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_13);


        verticalLayout_5->addLayout(horizontalLayout_12);

        stackedWidget->addWidget(looseScreen);
        page_4 = new QWidget();
        page_4->setObjectName(QString::fromUtf8("page_4"));
        label_4 = new QLabel(page_4);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(230, 10, 321, 31));
        QFont font5;
        font5.setPointSize(20);
        font5.setBold(true);
        font5.setWeight(75);
        label_4->setFont(font5);
        label_4->setStyleSheet(QString::fromUtf8("color: rgb(231, 231, 231);"));
        label_4->setAlignment(Qt::AlignCenter);
        verticalLayoutWidget_4 = new QWidget(page_4);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(210, 480, 361, 116));
        verticalLayout_7 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        rematchISurrendered = new QPushButton(verticalLayoutWidget_4);
        rematchISurrendered->setObjectName(QString::fromUtf8("rematchISurrendered"));
        rematchISurrendered->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_7->addWidget(rematchISurrendered);

        menuSurrenderOwn = new QPushButton(verticalLayoutWidget_4);
        menuSurrenderOwn->setObjectName(QString::fromUtf8("menuSurrenderOwn"));
        menuSurrenderOwn->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_7->addWidget(menuSurrenderOwn);

        quitSurrenderOwn = new QPushButton(verticalLayoutWidget_4);
        quitSurrenderOwn->setObjectName(QString::fromUtf8("quitSurrenderOwn"));
        quitSurrenderOwn->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_7->addWidget(quitSurrenderOwn);

        horizontalLayoutWidget_6 = new QWidget(page_4);
        horizontalLayoutWidget_6->setObjectName(QString::fromUtf8("horizontalLayoutWidget_6"));
        horizontalLayoutWidget_6->setGeometry(QRect(60, 50, 711, 421));
        gifSurrenderOwn = new QHBoxLayout(horizontalLayoutWidget_6);
        gifSurrenderOwn->setObjectName(QString::fromUtf8("gifSurrenderOwn"));
        gifSurrenderOwn->setContentsMargins(0, 0, 0, 0);
        stackedWidget->addWidget(page_4);
        page_5 = new QWidget();
        page_5->setObjectName(QString::fromUtf8("page_5"));
        label_5 = new QLabel(page_5);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(230, 10, 311, 31));
        label_5->setFont(font5);
        label_5->setStyleSheet(QString::fromUtf8("color: rgb(234, 234, 234);"));
        verticalLayoutWidget_6 = new QWidget(page_5);
        verticalLayoutWidget_6->setObjectName(QString::fromUtf8("verticalLayoutWidget_6"));
        verticalLayoutWidget_6->setGeometry(QRect(250, 480, 301, 116));
        verticalLayout_8 = new QVBoxLayout(verticalLayoutWidget_6);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        rematchOppSurrendered = new QPushButton(verticalLayoutWidget_6);
        rematchOppSurrendered->setObjectName(QString::fromUtf8("rematchOppSurrendered"));
        rematchOppSurrendered->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_8->addWidget(rematchOppSurrendered);

        menuSurrenderOpp = new QPushButton(verticalLayoutWidget_6);
        menuSurrenderOpp->setObjectName(QString::fromUtf8("menuSurrenderOpp"));
        menuSurrenderOpp->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_8->addWidget(menuSurrenderOpp);

        quitSurrenderOpp = new QPushButton(verticalLayoutWidget_6);
        quitSurrenderOpp->setObjectName(QString::fromUtf8("quitSurrenderOpp"));
        quitSurrenderOpp->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        verticalLayout_8->addWidget(quitSurrenderOpp);

        horizontalLayoutWidget_7 = new QWidget(page_5);
        horizontalLayoutWidget_7->setObjectName(QString::fromUtf8("horizontalLayoutWidget_7"));
        horizontalLayoutWidget_7->setGeometry(QRect(140, 60, 541, 351));
        gifOppSurrendered = new QHBoxLayout(horizontalLayoutWidget_7);
        gifOppSurrendered->setObjectName(QString::fromUtf8("gifOppSurrendered"));
        gifOppSurrendered->setContentsMargins(0, 0, 0, 0);
        stackedWidget->addWidget(page_5);
        page_6 = new QWidget();
        page_6->setObjectName(QString::fromUtf8("page_6"));
        label_17 = new QLabel(page_6);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(300, 110, 161, 51));
        label_17->setFont(font5);
        label_17->setStyleSheet(QString::fromUtf8("color: rgb(239, 239, 239);"));
        label_17->setAlignment(Qt::AlignCenter);
        horizontalLayoutWidget_3 = new QWidget(page_6);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(130, 220, 501, 141));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        rematchYes = new QPushButton(horizontalLayoutWidget_3);
        rematchYes->setObjectName(QString::fromUtf8("rematchYes"));
        rematchYes->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        horizontalLayout_3->addWidget(rematchYes);

        rematchNo = new QPushButton(horizontalLayoutWidget_3);
        rematchNo->setObjectName(QString::fromUtf8("rematchNo"));
        rematchNo->setStyleSheet(QString::fromUtf8("background-color: rgb(231, 231, 231);"));

        horizontalLayout_3->addWidget(rematchNo);

        stackedWidget->addWidget(page_6);
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        verticalLayoutWidget_7 = new QWidget(page_3);
        verticalLayoutWidget_7->setObjectName(QString::fromUtf8("verticalLayoutWidget_7"));
        verticalLayoutWidget_7->setGeometry(QRect(10, 10, 801, 601));
        verticalLayout_9 = new QVBoxLayout(verticalLayoutWidget_7);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        horizontalLayout_13->setSizeConstraint(QLayout::SetMinimumSize);
        horizontalSpacer_15 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_15);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_10->addItem(verticalSpacer_7);

        lineEdit_2 = new QLineEdit(verticalLayoutWidget_7);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lineEdit_2->sizePolicy().hasHeightForWidth());
        lineEdit_2->setSizePolicy(sizePolicy);
        lineEdit_2->setMinimumSize(QSize(0, 50));
        lineEdit_2->setMaximumSize(QSize(50, 50));

        verticalLayout_10->addWidget(lineEdit_2);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_10->addItem(verticalSpacer_8);


        horizontalLayout_13->addLayout(verticalLayout_10);

        horizontalSpacer_16 = new QSpacerItem(200, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_16);

        choiceLayout_2 = new QHBoxLayout();
        choiceLayout_2->setObjectName(QString::fromUtf8("choiceLayout_2"));
        choiceLayout_2->setSizeConstraint(QLayout::SetFixedSize);

        horizontalLayout_13->addLayout(choiceLayout_2);

        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_17);

        quitGame_2 = new QPushButton(verticalLayoutWidget_7);
        quitGame_2->setObjectName(QString::fromUtf8("quitGame_2"));
        quitGame_2->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_13->addWidget(quitGame_2);

        horizontalSpacer_18 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_18);


        verticalLayout_9->addLayout(horizontalLayout_13);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        horizontalSpacer_19 = new QSpacerItem(150, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_19);

        boardPlaceholder_4 = new QGridLayout();
        boardPlaceholder_4->setSpacing(6);
        boardPlaceholder_4->setObjectName(QString::fromUtf8("boardPlaceholder_4"));
        boardPlaceholder_4->setSizeConstraint(QLayout::SetMinimumSize);

        horizontalLayout_14->addLayout(boardPlaceholder_4);

        horizontalSpacer_20 = new QSpacerItem(150, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_20);


        verticalLayout_9->addLayout(horizontalLayout_14);

        test_2 = new QPushButton(verticalLayoutWidget_7);
        test_2->setObjectName(QString::fromUtf8("test_2"));

        verticalLayout_9->addWidget(test_2);

        stackedWidget->addWidget(page_3);
        StartScreen = new QWidget();
        StartScreen->setObjectName(QString::fromUtf8("StartScreen"));
        label_20 = new QLabel(StartScreen);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setGeometry(QRect(230, 0, 361, 121));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Open Sans Extrabold"));
        font6.setPointSize(48);
        font6.setBold(true);
        font6.setItalic(false);
        font6.setWeight(75);
        label_20->setFont(font6);
        label_20->setStyleSheet(QString::fromUtf8("background-color: rgb(47, 47, 47);\n"
"color: rgb(243, 243, 243);"));
        label_20->setAlignment(Qt::AlignCenter);
        toGame = new QPushButton(StartScreen);
        toGame->setObjectName(QString::fromUtf8("toGame"));
        toGame->setGeometry(QRect(280, 390, 261, 81));
        QFont font7;
        font7.setFamily(QString::fromUtf8("Open Sans Extrabold"));
        font7.setPointSize(24);
        font7.setBold(true);
        font7.setWeight(75);
        toGame->setFont(font7);
        toGame->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);\n"
"border-color: rgb(243, 243, 243);"));
        label_21 = new QLabel(StartScreen);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setGeometry(QRect(360, 170, 221, 271));
        label_21->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/King_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_22 = new QLabel(StartScreen);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setGeometry(QRect(470, 100, 221, 271));
        label_22->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Bishop_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_23 = new QLabel(StartScreen);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setGeometry(QRect(560, 160, 221, 271));
        label_23->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Knight_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_24 = new QLabel(StartScreen);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setGeometry(QRect(620, 50, 221, 271));
        label_24->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_35 = new QLabel(StartScreen);
        label_35->setObjectName(QString::fromUtf8("label_35"));
        label_35->setGeometry(QRect(360, 100, 221, 271));
        label_35->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Queen_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_36 = new QLabel(StartScreen);
        label_36->setObjectName(QString::fromUtf8("label_36"));
        label_36->setGeometry(QRect(470, 240, 221, 271));
        label_36->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Rook_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_37 = new QLabel(StartScreen);
        label_37->setObjectName(QString::fromUtf8("label_37"));
        label_37->setGeometry(QRect(130, 80, 221, 271));
        label_37->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Bishop_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_38 = new QLabel(StartScreen);
        label_38->setObjectName(QString::fromUtf8("label_38"));
        label_38->setGeometry(QRect(260, 170, 221, 271));
        label_38->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/King_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_39 = new QLabel(StartScreen);
        label_39->setObjectName(QString::fromUtf8("label_39"));
        label_39->setGeometry(QRect(40, 160, 221, 271));
        label_39->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Knight_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_40 = new QLabel(StartScreen);
        label_40->setObjectName(QString::fromUtf8("label_40"));
        label_40->setGeometry(QRect(-60, 180, 221, 271));
        label_40->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_41 = new QLabel(StartScreen);
        label_41->setObjectName(QString::fromUtf8("label_41"));
        label_41->setGeometry(QRect(250, 80, 221, 271));
        label_41->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Queen_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_42 = new QLabel(StartScreen);
        label_42->setObjectName(QString::fromUtf8("label_42"));
        label_42->setGeometry(QRect(130, 230, 221, 271));
        label_42->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Rook_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        close = new QPushButton(StartScreen);
        close->setObjectName(QString::fromUtf8("close"));
        close->setGeometry(QRect(280, 480, 261, 81));
        close->setFont(font7);
        close->setStyleSheet(QString::fromUtf8("color: rgb(243, 243, 243);\n"
"border-color: rgb(243, 243, 243);"));
        label_43 = new QLabel(StartScreen);
        label_43->setObjectName(QString::fromUtf8("label_43"));
        label_43->setGeometry(QRect(-50, 70, 221, 271));
        label_43->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_44 = new QLabel(StartScreen);
        label_44->setObjectName(QString::fromUtf8("label_44"));
        label_44->setGeometry(QRect(40, 30, 221, 271));
        label_44->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_45 = new QLabel(StartScreen);
        label_45->setObjectName(QString::fromUtf8("label_45"));
        label_45->setGeometry(QRect(-50, -40, 221, 271));
        label_45->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_46 = new QLabel(StartScreen);
        label_46->setObjectName(QString::fromUtf8("label_46"));
        label_46->setGeometry(QRect(40, 270, 221, 271));
        label_46->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_47 = new QLabel(StartScreen);
        label_47->setObjectName(QString::fromUtf8("label_47"));
        label_47->setGeometry(QRect(-60, 300, 221, 271));
        label_47->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_48 = new QLabel(StartScreen);
        label_48->setObjectName(QString::fromUtf8("label_48"));
        label_48->setGeometry(QRect(550, 30, 221, 271));
        label_48->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_49 = new QLabel(StartScreen);
        label_49->setObjectName(QString::fromUtf8("label_49"));
        label_49->setGeometry(QRect(620, 300, 221, 271));
        label_49->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_50 = new QLabel(StartScreen);
        label_50->setObjectName(QString::fromUtf8("label_50"));
        label_50->setGeometry(QRect(560, 260, 221, 271));
        label_50->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_51 = new QLabel(StartScreen);
        label_51->setObjectName(QString::fromUtf8("label_51"));
        label_51->setGeometry(QRect(620, -40, 221, 271));
        label_51->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_52 = new QLabel(StartScreen);
        label_52->setObjectName(QString::fromUtf8("label_52"));
        label_52->setGeometry(QRect(620, 190, 221, 271));
        label_52->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_53 = new QLabel(StartScreen);
        label_53->setObjectName(QString::fromUtf8("label_53"));
        label_53->setGeometry(QRect(210, 570, 221, 271));
        label_53->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Pawn_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_54 = new QLabel(StartScreen);
        label_54->setObjectName(QString::fromUtf8("label_54"));
        label_54->setGeometry(QRect(120, 30, 221, 271));
        label_54->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Rook_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_55 = new QLabel(StartScreen);
        label_55->setObjectName(QString::fromUtf8("label_55"));
        label_55->setGeometry(QRect(130, 160, 221, 271));
        label_55->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Bishop_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_56 = new QLabel(StartScreen);
        label_56->setObjectName(QString::fromUtf8("label_56"));
        label_56->setGeometry(QRect(40, 110, 221, 271));
        label_56->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Knight_black_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);\n"
"border: 0px;"));
        label_57 = new QLabel(StartScreen);
        label_57->setObjectName(QString::fromUtf8("label_57"));
        label_57->setGeometry(QRect(470, 160, 221, 271));
        label_57->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Bishop_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_58 = new QLabel(StartScreen);
        label_58->setObjectName(QString::fromUtf8("label_58"));
        label_58->setGeometry(QRect(470, 40, 221, 271));
        label_58->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Rook_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        label_59 = new QLabel(StartScreen);
        label_59->setObjectName(QString::fromUtf8("label_59"));
        label_59->setGeometry(QRect(560, 110, 221, 271));
        label_59->setStyleSheet(QString::fromUtf8("image: url(:/new/prefix1/Knight_white_noSub.png);\n"
"background-color: rgb(255, 255, 255, 0%);"));
        stackedWidget->addWidget(StartScreen);
        label_57->raise();
        label_20->raise();
        label_58->raise();
        label_44->raise();
        label_56->raise();
        label_54->raise();
        label_41->raise();
        label_21->raise();
        label_36->raise();
        label_38->raise();
        label_37->raise();
        label_39->raise();
        label_40->raise();
        label_22->raise();
        label_23->raise();
        label_24->raise();
        label_43->raise();
        label_45->raise();
        label_46->raise();
        label_47->raise();
        label_48->raise();
        label_49->raise();
        label_50->raise();
        label_51->raise();
        label_52->raise();
        label_53->raise();
        label_55->raise();
        label_59->raise();
        label_42->raise();
        label_35->raise();
        toGame->raise();
        close->raise();

        retranslateUi(Game);

        stackedWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(Game);
    } // setupUi

    void retranslateUi(QWidget *Game)
    {
        Game->setWindowTitle(QCoreApplication::translate("Game", "Game", nullptr));
        label->setText(QCoreApplication::translate("Game", "SETUP", nullptr));
        group->setPlaceholderText(QCoreApplication::translate("Game", "6", nullptr));
        name->setPlaceholderText(QCoreApplication::translate("Game", "Name", nullptr));
        port->setText(QCoreApplication::translate("Game", "1234", nullptr));
        port->setPlaceholderText(QCoreApplication::translate("Game", "Port", nullptr));
        ip->setText(QCoreApplication::translate("Game", "localhost", nullptr));
        ip->setPlaceholderText(QCoreApplication::translate("Game", "IP-Adress", nullptr));
        groupBox->setTitle(QCoreApplication::translate("Game", "Connectivity", nullptr));
        server->setText(QCoreApplication::translate("Game", "Ser&ver", nullptr));
        client->setText(QCoreApplication::translate("Game", "Client", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("Game", "Color", nullptr));
        youBegin->setText(QCoreApplication::translate("Game", "White", nullptr));
        opponentBegins->setText(QCoreApplication::translate("Game", "Blac&k", nullptr));
        randomBeginner->setText(QCoreApplication::translate("Game", "Random", nullptr));
        start->setText(QCoreApplication::translate("Game", "Start", nullptr));
        quit->setText(QCoreApplication::translate("Game", "Quit", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("Game", "Texture Package", nullptr));
        label_33->setText(QString());
        label_34->setText(QString());
        normalTexture->setText(QCoreApplication::translate("Game", "Nor&mal", nullptr));
        animalTexture->setText(QCoreApplication::translate("Game", "A&nimals", nullptr));
        label_3->setText(QCoreApplication::translate("Game", "Gruppe:", nullptr));
        label_6->setText(QCoreApplication::translate("Game", "Gruppe:", nullptr));
        label_2->setText(QCoreApplication::translate("Game", "Turns:", nullptr));
        label_13->setText(QCoreApplication::translate("Game", "Turns:", nullptr));
        timer->setText(QString());
        label_14->setText(QCoreApplication::translate("Game", "Timer:", nullptr));
        label_7->setText(QCoreApplication::translate("Game", "A", nullptr));
        label_8->setText(QCoreApplication::translate("Game", "B", nullptr));
        label_9->setText(QCoreApplication::translate("Game", "C", nullptr));
        label_10->setText(QCoreApplication::translate("Game", "D", nullptr));
        label_16->setText(QCoreApplication::translate("Game", "E", nullptr));
        label_11->setText(QCoreApplication::translate("Game", "F", nullptr));
        label_12->setText(QCoreApplication::translate("Game", "G", nullptr));
        label_15->setText(QCoreApplication::translate("Game", "H", nullptr));
        label_31->setText(QCoreApplication::translate("Game", "8", nullptr));
        label_25->setText(QCoreApplication::translate("Game", "7", nullptr));
        label_32->setText(QCoreApplication::translate("Game", "6", nullptr));
        label_27->setText(QCoreApplication::translate("Game", "5", nullptr));
        label_29->setText(QCoreApplication::translate("Game", "4", nullptr));
        label_26->setText(QCoreApplication::translate("Game", "3", nullptr));
        label_28->setText(QCoreApplication::translate("Game", "2", nullptr));
        label_30->setText(QCoreApplication::translate("Game", "1", nullptr));
        pointsTop->setText(QCoreApplication::translate("Game", "0", nullptr));
        pointsBottom->setText(QCoreApplication::translate("Game", "0", nullptr));
        label_18->setText(QCoreApplication::translate("Game", "Points:", nullptr));
        label_19->setText(QCoreApplication::translate("Game", "Points:", nullptr));
        quitGame->setText(QCoreApplication::translate("Game", "Surrender", nullptr));
        back->setText(QCoreApplication::translate("Game", "Back", nullptr));
        openChat->setText(QCoreApplication::translate("Game", "Chat", nullptr));
        lastSceneWin->setText(QCoreApplication::translate("Game", "Last Scene", nullptr));
        rematchWin->setText(QCoreApplication::translate("Game", "Rematch", nullptr));
        startWin->setText(QCoreApplication::translate("Game", "Start Screen", nullptr));
        quitWin->setText(QCoreApplication::translate("Game", "Quit", nullptr));
        lastSceneLoose->setText(QCoreApplication::translate("Game", "Last Scene", nullptr));
        rematchLoose->setText(QCoreApplication::translate("Game", "Rematch", nullptr));
        startLost->setText(QCoreApplication::translate("Game", "Start Screen", nullptr));
        quitLost->setText(QCoreApplication::translate("Game", "Quit", nullptr));
        label_4->setText(QCoreApplication::translate("Game", "SURRENDERED", nullptr));
        rematchISurrendered->setText(QCoreApplication::translate("Game", "Rematch", nullptr));
        menuSurrenderOwn->setText(QCoreApplication::translate("Game", "Back to Menu", nullptr));
        quitSurrenderOwn->setText(QCoreApplication::translate("Game", "Quit", nullptr));
        label_5->setText(QCoreApplication::translate("Game", "Opponent surrendered", nullptr));
        rematchOppSurrendered->setText(QCoreApplication::translate("Game", "Rematch", nullptr));
        menuSurrenderOpp->setText(QCoreApplication::translate("Game", "Back to Menu", nullptr));
        quitSurrenderOpp->setText(QCoreApplication::translate("Game", "Quit", nullptr));
        label_17->setText(QCoreApplication::translate("Game", "Rematch?", nullptr));
        rematchYes->setText(QCoreApplication::translate("Game", "Yes", nullptr));
        rematchNo->setText(QCoreApplication::translate("Game", "No", nullptr));
        quitGame_2->setText(QCoreApplication::translate("Game", "Quit", nullptr));
        test_2->setText(QCoreApplication::translate("Game", "Test Bauernumwandlung", nullptr));
        label_20->setText(QCoreApplication::translate("Game", "CHESS", nullptr));
        toGame->setText(QCoreApplication::translate("Game", "Start", nullptr));
        label_21->setText(QString());
        label_22->setText(QString());
        label_23->setText(QString());
        label_24->setText(QString());
        label_35->setText(QString());
        label_36->setText(QString());
        label_37->setText(QString());
        label_38->setText(QString());
        label_39->setText(QString());
        label_40->setText(QString());
        label_41->setText(QString());
        label_42->setText(QString());
        close->setText(QCoreApplication::translate("Game", "Quit", nullptr));
        label_43->setText(QString());
        label_44->setText(QString());
        label_45->setText(QString());
        label_46->setText(QString());
        label_47->setText(QString());
        label_48->setText(QString());
        label_49->setText(QString());
        label_50->setText(QString());
        label_51->setText(QString());
        label_52->setText(QString());
        label_53->setText(QString());
        label_54->setText(QString());
        label_55->setText(QString());
        label_56->setText(QString());
        label_57->setText(QString());
        label_58->setText(QString());
        label_59->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Game: public Ui_Game {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAME_H
