/****************************************************************************
** Meta object code from reading C++ file 'chesstile.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../cpp_19sose_g6/Gui/chesstile.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'chesstile.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ChessTile_t {
    QByteArrayData data[19];
    char stringdata0[211];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ChessTile_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ChessTile_t qt_meta_stringdata_ChessTile = {
    {
QT_MOC_LITERAL(0, 0, 9), // "ChessTile"
QT_MOC_LITERAL(1, 10, 7), // "toggled"
QT_MOC_LITERAL(2, 18, 0), // ""
QT_MOC_LITERAL(3, 19, 10), // "identifier"
QT_MOC_LITERAL(4, 30, 20), // "transformationToggle"
QT_MOC_LITERAL(5, 51, 5), // "color"
QT_MOC_LITERAL(6, 57, 12), // "_2identifier"
QT_MOC_LITERAL(7, 70, 7), // "hovered"
QT_MOC_LITERAL(8, 78, 4), // "left"
QT_MOC_LITERAL(9, 83, 12), // "checkedClick"
QT_MOC_LITERAL(10, 96, 15), // "clickedReaction"
QT_MOC_LITERAL(11, 112, 21), // "clickedTransformation"
QT_MOC_LITERAL(12, 134, 10), // "enterEvent"
QT_MOC_LITERAL(13, 145, 7), // "QEvent*"
QT_MOC_LITERAL(14, 153, 1), // "e"
QT_MOC_LITERAL(15, 155, 14), // "colorWhenHover"
QT_MOC_LITERAL(16, 170, 16), // "uncolorWhenleave"
QT_MOC_LITERAL(17, 187, 10), // "leaveEvent"
QT_MOC_LITERAL(18, 198, 12) // "checkEnabled"

    },
    "ChessTile\0toggled\0\0identifier\0"
    "transformationToggle\0color\0_2identifier\0"
    "hovered\0left\0checkedClick\0clickedReaction\0"
    "clickedTransformation\0enterEvent\0"
    "QEvent*\0e\0colorWhenHover\0uncolorWhenleave\0"
    "leaveEvent\0checkEnabled"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ChessTile[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,
       4,    3,   77,    2, 0x06 /* Public */,
       7,    0,   84,    2, 0x06 /* Public */,
       8,    0,   85,    2, 0x06 /* Public */,
       9,    0,   86,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    0,   87,    2, 0x0a /* Public */,
      11,    0,   88,    2, 0x0a /* Public */,
      12,    1,   89,    2, 0x0a /* Public */,
      15,    0,   92,    2, 0x0a /* Public */,
      16,    0,   93,    2, 0x0a /* Public */,
      17,    1,   94,    2, 0x0a /* Public */,
      18,    0,   97,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,    3,    5,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,

       0        // eod
};

void ChessTile::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ChessTile *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->toggled((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->transformationToggle((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->hovered(); break;
        case 3: _t->left(); break;
        case 4: _t->checkedClick(); break;
        case 5: _t->clickedReaction(); break;
        case 6: _t->clickedTransformation(); break;
        case 7: _t->enterEvent((*reinterpret_cast< QEvent*(*)>(_a[1]))); break;
        case 8: _t->colorWhenHover(); break;
        case 9: _t->uncolorWhenleave(); break;
        case 10: _t->leaveEvent((*reinterpret_cast< QEvent*(*)>(_a[1]))); break;
        case 11: _t->checkEnabled(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ChessTile::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ChessTile::toggled)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ChessTile::*)(int , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ChessTile::transformationToggle)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ChessTile::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ChessTile::hovered)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (ChessTile::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ChessTile::left)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (ChessTile::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ChessTile::checkedClick)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ChessTile::staticMetaObject = { {
    &QPushButton::staticMetaObject,
    qt_meta_stringdata_ChessTile.data,
    qt_meta_data_ChessTile,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ChessTile::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ChessTile::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ChessTile.stringdata0))
        return static_cast<void*>(this);
    return QPushButton::qt_metacast(_clname);
}

int ChessTile::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QPushButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void ChessTile::toggled(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ChessTile::transformationToggle(int _t1, int _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void ChessTile::hovered()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void ChessTile::left()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void ChessTile::checkedClick()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
