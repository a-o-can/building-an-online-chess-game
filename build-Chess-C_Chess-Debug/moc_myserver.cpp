/****************************************************************************
** Meta object code from reading C++ file 'myserver.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../cpp_19sose_g6/Netzwerk/myserver.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'myserver.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MyServer_t {
    QByteArrayData data[41];
    char stringdata0[387];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MyServer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MyServer_t qt_meta_stringdata_MyServer = {
    {
QT_MOC_LITERAL(0, 0, 8), // "MyServer"
QT_MOC_LITERAL(1, 9, 9), // "startInfo"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 2), // "we"
QT_MOC_LITERAL(4, 23, 11), // "beginnender"
QT_MOC_LITERAL(5, 35, 12), // "gegnernummer"
QT_MOC_LITERAL(6, 48, 12), // "gegnernumber"
QT_MOC_LITERAL(7, 61, 6), // "gegner"
QT_MOC_LITERAL(8, 68, 10), // "sendVector"
QT_MOC_LITERAL(9, 79, 2), // "sX"
QT_MOC_LITERAL(10, 82, 2), // "sY"
QT_MOC_LITERAL(11, 85, 2), // "zX"
QT_MOC_LITERAL(12, 88, 2), // "zY"
QT_MOC_LITERAL(13, 91, 6), // "status"
QT_MOC_LITERAL(14, 98, 10), // "umwandlung"
QT_MOC_LITERAL(15, 109, 14), // "responseToMove"
QT_MOC_LITERAL(16, 124, 3), // "tmp"
QT_MOC_LITERAL(17, 128, 8), // "aufgeben"
QT_MOC_LITERAL(18, 137, 16), // "serverDisconnect"
QT_MOC_LITERAL(19, 154, 4), // "remi"
QT_MOC_LITERAL(20, 159, 9), // "okRematch"
QT_MOC_LITERAL(21, 169, 9), // "noRematch"
QT_MOC_LITERAL(22, 179, 4), // "chat"
QT_MOC_LITERAL(23, 184, 9), // "nachricht"
QT_MOC_LITERAL(24, 194, 8), // "sendName"
QT_MOC_LITERAL(25, 203, 4), // "name"
QT_MOC_LITERAL(26, 208, 13), // "lostConnexion"
QT_MOC_LITERAL(27, 222, 11), // "recieveName"
QT_MOC_LITERAL(28, 234, 11), // "sessionOpen"
QT_MOC_LITERAL(29, 246, 8), // "sendData"
QT_MOC_LITERAL(30, 255, 9), // "writeData"
QT_MOC_LITERAL(31, 265, 19), // "std::vector<quint8>"
QT_MOC_LITERAL(32, 285, 10), // "movevector"
QT_MOC_LITERAL(33, 296, 7), // "rematch"
QT_MOC_LITERAL(34, 304, 14), // "writeOkRematch"
QT_MOC_LITERAL(35, 319, 14), // "writeNoRematch"
QT_MOC_LITERAL(36, 334, 12), // "writeMessage"
QT_MOC_LITERAL(37, 347, 3), // "msg"
QT_MOC_LITERAL(38, 351, 10), // "disconnect"
QT_MOC_LITERAL(39, 362, 9), // "writeName"
QT_MOC_LITERAL(40, 372, 14) // "connetionError"

    },
    "MyServer\0startInfo\0\0we\0beginnender\0"
    "gegnernummer\0gegnernumber\0gegner\0"
    "sendVector\0sX\0sY\0zX\0zY\0status\0umwandlung\0"
    "responseToMove\0tmp\0aufgeben\0"
    "serverDisconnect\0remi\0okRematch\0"
    "noRematch\0chat\0nachricht\0sendName\0"
    "name\0lostConnexion\0recieveName\0"
    "sessionOpen\0sendData\0writeData\0"
    "std::vector<quint8>\0movevector\0rematch\0"
    "writeOkRematch\0writeNoRematch\0"
    "writeMessage\0msg\0disconnect\0writeName\0"
    "connetionError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MyServer[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,  129,    2, 0x06 /* Public */,
       6,    1,  136,    2, 0x06 /* Public */,
       8,    6,  139,    2, 0x06 /* Public */,
      15,    1,  152,    2, 0x06 /* Public */,
      17,    0,  155,    2, 0x06 /* Public */,
      18,    0,  156,    2, 0x06 /* Public */,
      19,    0,  157,    2, 0x06 /* Public */,
      20,    0,  158,    2, 0x06 /* Public */,
      21,    0,  159,    2, 0x06 /* Public */,
      22,    1,  160,    2, 0x06 /* Public */,
      24,    1,  163,    2, 0x06 /* Public */,
      26,    0,  166,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      27,    1,  167,    2, 0x0a /* Public */,
      28,    0,  170,    2, 0x0a /* Public */,
      29,    0,  171,    2, 0x0a /* Public */,
      30,    1,  172,    2, 0x0a /* Public */,
      33,    0,  175,    2, 0x0a /* Public */,
      34,    0,  176,    2, 0x0a /* Public */,
      35,    0,  177,    2, 0x0a /* Public */,
      36,    2,  178,    2, 0x0a /* Public */,
      38,    0,  183,    2, 0x0a /* Public */,
      39,    0,  184,    2, 0x0a /* Public */,
      40,    0,  185,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::UChar, QMetaType::UChar,    3,    4,    5,
    QMetaType::Void, QMetaType::UChar,    7,
    QMetaType::Void, QMetaType::UChar, QMetaType::UChar, QMetaType::UChar, QMetaType::UChar, QMetaType::Int, QMetaType::Int,    9,   10,   11,   12,   13,   14,
    QMetaType::Void, QMetaType::UChar,   16,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   23,
    QMetaType::Void, QMetaType::QString,   25,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,   25,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 31,   32,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Bool,   37,   25,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MyServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MyServer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->startInfo((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< quint8(*)>(_a[2])),(*reinterpret_cast< quint8(*)>(_a[3]))); break;
        case 1: _t->gegnernumber((*reinterpret_cast< quint8(*)>(_a[1]))); break;
        case 2: _t->sendVector((*reinterpret_cast< quint8(*)>(_a[1])),(*reinterpret_cast< quint8(*)>(_a[2])),(*reinterpret_cast< quint8(*)>(_a[3])),(*reinterpret_cast< quint8(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< int(*)>(_a[6]))); break;
        case 3: _t->responseToMove((*reinterpret_cast< quint8(*)>(_a[1]))); break;
        case 4: _t->aufgeben(); break;
        case 5: _t->serverDisconnect(); break;
        case 6: _t->remi(); break;
        case 7: _t->okRematch(); break;
        case 8: _t->noRematch(); break;
        case 9: _t->chat((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->sendName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->lostConnexion(); break;
        case 12: _t->recieveName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 13: _t->sessionOpen(); break;
        case 14: _t->sendData(); break;
        case 15: _t->writeData((*reinterpret_cast< std::vector<quint8>(*)>(_a[1]))); break;
        case 16: _t->rematch(); break;
        case 17: _t->writeOkRematch(); break;
        case 18: _t->writeNoRematch(); break;
        case 19: _t->writeMessage((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 20: _t->disconnect(); break;
        case 21: _t->writeName(); break;
        case 22: _t->connetionError(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MyServer::*)(int , quint8 , quint8 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::startInfo)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (MyServer::*)(quint8 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::gegnernumber)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (MyServer::*)(quint8 , quint8 , quint8 , quint8 , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::sendVector)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (MyServer::*)(quint8 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::responseToMove)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (MyServer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::aufgeben)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (MyServer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::serverDisconnect)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (MyServer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::remi)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (MyServer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::okRematch)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (MyServer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::noRematch)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (MyServer::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::chat)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (MyServer::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::sendName)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (MyServer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MyServer::lostConnexion)) {
                *result = 11;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MyServer::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_MyServer.data,
    qt_meta_data_MyServer,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MyServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MyServer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MyServer.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int MyServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 23;
    }
    return _id;
}

// SIGNAL 0
void MyServer::startInfo(int _t1, quint8 _t2, quint8 _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MyServer::gegnernumber(quint8 _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MyServer::sendVector(quint8 _t1, quint8 _t2, quint8 _t3, quint8 _t4, int _t5, int _t6)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t4))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t5))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t6))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MyServer::responseToMove(quint8 _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MyServer::aufgeben()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void MyServer::serverDisconnect()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void MyServer::remi()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void MyServer::okRematch()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void MyServer::noRematch()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void MyServer::chat(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void MyServer::sendName(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void MyServer::lostConnexion()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
