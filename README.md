# CPP_19SoSe_G6

## Authors

* **Paul** -        *GUI*
* **Oguz** -        *NETWORK*
* **Rayene** -      *LOGIK*


## Chess Project

|***files/dirs***|***description***|
|-----|-----------|
|[`Gui`](/Gui)|Gui folder|
|[`Logik`](/Logik)|Logic part|
|[`Netzwerk`](/Netzwerk)|Network folder|
|[`Supervisor`](/superveisor.h)|Class for connecting all parts of the project|
|[`main.cpp`](/main.cpp)|main file to start the game|


