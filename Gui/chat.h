#ifndef CHAT_H
#define CHAT_H

#include <QDialog>
#include <QString>
namespace Ui {
class Chat;
}

class Chat : public QDialog
{
    Q_OBJECT

public:
    explicit Chat(QWidget *parent = 0);
    ~Chat();
    void setName(){_name=true;}
    void setMyName(QString name){_Name=name;}
    QString getOppName(){return _nameOpp;}
public slots:
    void readMsg();
    void nameOpp(QString nameOpp);
    void chat(QString msg);
    void chatOwn(QString msg, bool _name);
signals:
    void writeMsg(QString msg, bool name);

private:
    Ui::Chat *ui;
    bool _name;
    QString _Name;
    QString _nameOpp;
};

#endif // CHAT_H
