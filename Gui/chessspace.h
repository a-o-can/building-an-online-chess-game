#ifndef CHESSSPACE_H
#define CHESSSPACE_H
#include <QWidget>
#include "game.h"

class ChessSpace: public Game
{
    Q_OBJECT
public:
    ChessSpace(QWidget *parent = nullptr);
};

#endif // CHESSSPACE_H
