#ifndef SUPERVISOR_H
#define SUPERVISOR_H

#include <QObject>
#include <QWidget>
#include <QDebug>
#include "Netzwerk/myserver.h"
#include "Netzwerk/myclient.h"
#include "Logik/chess.h"
#include "Gui/game.h"

class Supervisor : public QWidget
{
    Q_OBJECT

public:
    Supervisor();
    ~Supervisor();
    Game* game = nullptr;
    MyServer* server = nullptr;
    MyClient* client = nullptr;
    Chess* chess = nullptr;

public slots:
    void initClient(quint16 port, QString ip );
    void initServer(quint16 port, int begins);
signals:
    void test(quint16,QString);

private:

};

#endif // SUPERVISOR_H

